package co.jgdavis.simpletemplate;

public class InvalidTagException extends Exception {

	public InvalidTagException(String errMsg) {
		super(errMsg);
	}

}
