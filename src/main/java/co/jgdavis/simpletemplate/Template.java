package co.jgdavis.simpletemplate;

import java.io.*;
import java.util.Map;

public class Template {

	private String template;

	public Template(String template) {
		this.template = template;
	}

	public Template(File templateFile) throws IOException {
		readFile(templateFile);
	}

	public void generate(Map<String, Object> data, Writer out) throws IOException, InvalidTagException {
		StringReader reader = new StringReader(template);
		TemplateUtils.processTemplate(data, reader, out);
		reader.close();
	}

	private void readFile(File file) throws IOException {
		FileReader fileReader = new FileReader(file);
		BufferedReader reader = new BufferedReader(fileReader);
		StringBuffer buffer = new StringBuffer();
		while (reader.ready()) {
			buffer.append(reader.readLine());
		}
		this.template = buffer.toString();
	}

}
