package co.jgdavis.simpletemplate;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;

public class TemplateUtils {

	private static final int BUFFER_SIZE = 2048;
	private static final int MAX_TAG_LENGTH = BUFFER_SIZE / 2;
	private static final char ESCAPE_CHAR = '$';
	private static final char SEPARATOR_CHAR = '.';

	private static int findTagStart(char[] buf, int offset) {
		for (int i = offset; i < buf.length-1; ++i) {
			if (buf[i] == ESCAPE_CHAR && buf[i+1] == '{') {
				return i;
			}
		}
		return -1;
	}

	private static int findTagEnd(char[] buf, int offset) {
		for (int i = offset; i < buf.length; ++i) {
			if (buf[i] == '}') {
				return i;
			}
		}
		return -1;
	}

	private static String getTag(char[] buf, int tagStart, int tagEnd) {
		StringBuilder builder = new StringBuilder();
		for (int i = tagStart+2; i < tagEnd; ++i) {
			builder.append(buf[i]);
		}
		return builder.toString();
	}

	private static String parseTag(Map<String, Object> data, String tag) throws InvalidTagException {
		int separator = tag.indexOf(SEPARATOR_CHAR);
		if (separator > -1) {
			String prefix = tag.substring(0, separator);
			String suffix = tag.substring(separator + 1);
			if (!data.containsKey(prefix)) {
				throw new InvalidTagException("Template tag \"" + prefix + "\" does not exist in the provided data map");
			}
			return parseTag((Map<String, Object>) data.get(prefix), suffix);
		}

		if (!data.containsKey(tag)) {
			throw new InvalidTagException("Template tag \"" + tag + "\" does not exist in the provided data map");
		}
		return data.get(tag).toString();
	}

	public static void processTemplate(Map<String, Object> data, Reader in, Writer out) throws IOException, InvalidTagException {
		char[] buf = new char[BUFFER_SIZE];
		int length;
		while ((length = in.read(buf)) != -1) {
			int offset = 0;    // Last portion of buffer written
			int tagStart = findTagStart(buf, offset);
			while (tagStart > -1) {
				out.write(buf, offset, tagStart - offset);    // Write up to start of tag

				// If the tag may not fit in the buffer, move bytes to beginning of buffer and read in the rest
				if (tagStart >= MAX_TAG_LENGTH && in.ready()) {
					int transferSize = buf.length - tagStart;
					for (int i = 0; i < transferSize; ++i) {
						buf[i] = buf[i + tagStart];
					}
					tagStart = 0;    // Tag is now the first element
					length = transferSize + in.read(buf, transferSize, buf.length - transferSize);
				}

				int tagEnd = findTagEnd(buf, tagStart);
				if (tagEnd > -1) {
					String tag = getTag(buf, tagStart, tagEnd);
					String replacement = parseTag(data, tag);
					out.write(replacement);
					offset = tagEnd + 1;
				} else {
					out.write(buf, tagStart, 1);    // Write tag start character and continue
					offset = tagStart + 1;
				}

				tagStart = findTagStart(buf, offset);
			}
			out.write(buf, offset, length - offset); // Write the rest of the buffer
			out.flush();
		}
	}
}
